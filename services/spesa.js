const db = require('./db');
const helper = require('../helper');
const config = require('../config');
var debug=require('debug')('listaspesaapi:spesa');

async function getAll(page = 1){
  const offset = helper.getOffset(page, config.listPerPage);
  const rows = await db.query(
    `SELECT idLista, contenuto
    FROM listeSpesa LIMIT ?,?`, 
    [offset, config.listPerPage]
  );
  const data = helper.emptyOrRows(rows);
  const meta = {page};
  //const data=[{"contenuto":{"items": [{"id": "1", "name": "pane", "quantity": "1"}]}}];
  //const meta ={}
  return {
    data,
    meta
  }
}
async function setAll(items){
  var a=JSON.stringify(items);
  //var a=JSON.parse(items);
  //"UPDATE listeSpesa SET  contenuto="+items+" WHERE idLista=1"
const rows =  await db.query(
  "UPDATE listeSpesa SET  contenuto='"+a+"' WHERE idLista=1"
);

  return;
}
async function setPreferred(items){
 // var a=JSON.stringify(items);
  //var a=JSON.parse(items);
  //"UPDATE listeSpesa SET  contenuto="+items+" WHERE idLista=1"
/*const rows =  await db.query(
  "UPDATE listeSpesa SET  contenuto='"+a+"' WHERE idLista=1"
);*/
const rows =  await db.query(
  "SELECT contenuto FROM preferiti"
);

//{name,qty,timestamp}
debug("preferred",items);
debug("preferred",rows[0].contenuto)
var preferitiSalvati=rows[0].contenuto;
items.forEach(element => {
  var trovato=0;
  for (let i = 0; i < preferitiSalvati.length; i++) {
    if (element.toLowerCase()==preferitiSalvati[i].name) {
      preferitiSalvati[i].quantity++;
      preferitiSalvati[i].timestamp=Date.now();
      trovato=1;
    }
    
  }
if (trovato==0) {
  preferitiSalvati.push({'name':element,quantity:1,timestamp:Date.now()});
}
});
debug("preferitiSalvati",preferitiSalvati)
const rows1 =  await db.query(
  "UPDATE preferiti SET  contenuto='"+JSON.stringify(preferitiSalvati)+"' WHERE id=1"
);
  return;
}
async function getPreferred(){
  debug("getPreferred");
  const rows =  await db.query(
    "SELECT contenuto FROM preferiti"
  );
  return helper.emptyOrRows(rows);
}

module.exports = {
    getAll,setAll,setPreferred,getPreferred
}