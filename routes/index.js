var express = require('express');
var debug=require('debug')('listaspesaapi:indexRouter');
var router = express.Router();
const users = require('../services/users');
const listeSpesa = require('../services/spesa');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({"ciao":"ciao"});
});
router.get('/utenti', async function(req, res, next) {
  try {
    res.json(await users.getMultiple(req.query.page));
  } catch (err) {
    debug(`errore nel recupero utenti `, err.message);
    next(err);
  }
});
router.get('/listaSpesa', async function(req, res, next) {
  try {
    res.json(await listeSpesa.getAll(req.query.page));
  } catch (err) {
    debug(`errore nel recupero liste`, err.message);
    next(err);
  }
});
router.post('/listaSpesa',async  function(req, res, next) {
  try {
    debug("ciao",req.body);

    res.json(await listeSpesa.setAll(req.body));
    //res.json(await listeSpesa.setAll(req.query.page));
  } catch (err) {
    debug(`errore nel impostazione liste`, err.message);
    next(err);
  }
});
router.post('/preferiti',async  function(req, res, next) {
  try {
    debug("preferiti",req.body);

    res.json(await listeSpesa.setPreferred(req.body));
    //res.json(await listeSpesa.setAll(req.query.page));
  } catch (err) {
    debug(`errore nel impostazione preferiti`, err.message);
    next(err);
  }
});
router.get('/preferiti',async  function(req, res, next) {
  try {
    debug("preferiti",req.body);

    res.json(await listeSpesa.getPreferred(req.body));
    //res.json(await listeSpesa.setAll(req.query.page));
  } catch (err) {
    debug(`errore nel impostazione preferiti`, err.message);
    next(err);
  }
});
module.exports = router;
